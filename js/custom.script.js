(function($) {
    "use strict";
    var revapi;
    var bigSliderImages = [];
    var shift = $(window).width() > 640 ? 550 : 125;
    var dishPost = '';
    	dishPost += '<div class="col-lg-4 col-md-4 col-sm-4">'
                    +    '<a href="blog-single.html">'
                    +        '<img src="images/events/1.jpg" alt="">'
                    +    '</a>'
                    +    '<div class="text-wrapper">'
                    +        '<a href="blog-single.html" class="name">Wedding in our restaurant</a>'
                    +        '<div class="date">12 July 2014</div>'
                    +        '<p>It all started when my sweet granny came to the shore of US in 1926. As the time passed by she decided to open una resturacione de la vega. Nullam vel suscipit erat. Fusce augue est, tempus vitae venenatis</p>'
                    +        '<a class="more" href="blog-single.html"><span></span>Read more</a>'
                    +    '</div>'
                    +'</div>'
                    +'<div class="col-lg-4 col-md-4 col-sm-4">'
                    +    '<div class="text-wrapper">'
                    +        '<a href="blog-single.html" class="name">Wine reception</a>'
                    +        '<div class="date">5 July 2014</div>'
                    +        '<p>It all started when my sweet granny came to the shore of US in 1926. As the time passed by she decided to open una resturacione de la vega. Nullam vel suscipit erat. Fusce augue est, tempus vitae venenatis</p>'
                    +        '<a class="more" href="blog-single.html"><span></span>Read more</a>'
                    +    '</div>'
                    +    '<a href="blog-single.html">'
                    +        '<img src="images/events/2.jpg" alt="">'
                    +    '</a>'
                    +'</div>'
                    +'<div class="col-lg-4 col-md-4 col-sm-4">'
                    +    '<a href="blog-single.html">'
                    +        '<img src="images/events/3.jpg" alt="">'
                    +    '</a>'
                    +    '<div class="text-wrapper">'
                    +        '<a href="blog-single.html" class="name">Day Italliano gourmet</a>'
                    +        '<div class="date">3 July 2014</div>'
                    +        '<p>It all started when my sweet granny came to the shore of US in 1926. As the time passed by she decided to open una resturacione de la vega. Nullam vel suscipit erat. Fusce augue est, tempus vitae venenatis</p>'
                    +        '<a class="more" href="blog-single.html"><span></span>Read more</a>'
                    +    '</div>'
                    +'</div>';
    var dish = '';
    	dish += '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/9.jpg">'
                +                '<img alt="" src="images/dish/9.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Mascorpone</a>'
                +        '<span class="price">$51</span>'
                +        '<p>Pasta, turkey meat, mint, cucumber</p>'
                +    '</div>'
                +    '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/10.jpg">'
                +                '<img alt="" src="images/dish/10.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Fish Pasta</a>'
                +        '<span class="price">$84</span>'
                +        '<p>Fish, tomatoes, cheese, nuts</p>'
                +    '</div>'
                +    '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/11.jpg">'
                +                '<img alt="" src="images/dish/11.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Mascorpone</a>'
                +        '<span class="price">$51</span>'
                +        '<p>Pasta, turkey meat, mint, cucumber</p>'
                +    '</div>'
                +    '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/12.jpg">'
                +                '<img alt="" src="images/dish/12.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Brazilla Pasta</a>'
                +        '<span class="price">$37</span>'
                +        '<p>Pasta, turkey meat, mint, cucumber</p>'
                +    '</div>';
        var dish2 = '';
    	dish2 += '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/13.jpg">'
                +                '<img alt="" src="images/dish/13.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Mascorpone</a>'
                +        '<span class="price">$51</span>'
                +        '<p>Pasta, turkey meat, mint, cucumber</p>'
                +    '</div>'
                +    '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/14.jpg">'
                +                '<img alt="" src="images/dish/14.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Mascorpone</a>'
                +        '<span class="price">$51</span>'
                +        '<p>Pasta, turkey meat, mint, cucumber</p>'
                +    '</div>'
                +    '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/15.jpg">'
                +                '<img alt="" src="images/dish/15.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Fish Pasta</a>'
                +        '<span class="price">$84</span>'
                +        '<p>Fish, tomatoes, cheese, nuts</p>'
                +    '</div>'
                +    '<div class="dish-block">'
                +        '<div class="image-wrapper">'
                +            '<a class="zoom" href="images/dish/16.jpg">'
                +                '<img alt="" src="images/dish/16.jpg">'
                +            '</a>'
                +        '</div>'
                +        '<a href="">Brazilla Pasta</a>'
                +        '<span class="price">$37</span>'
                +        '<p>Pasta, turkey meat, mint, cucumber</p>'
                +    '</div>';

	/*----------  NAVIGATION ON PAGE  ----------*/
	$.scrollIt();
	/*----------  //NAVIGATION ON PAGE  ----------*/


    $('#showMore a').on('click', function(){
    	if(dish.length){
    		$('#grid').append(dish);	
    	}else if(!dish2.length) {
    		return false
    	}else{
    		$('#grid').append(dish2);    		
    	}

		setTimeout(function(){
			if(!dish.length){
				$('#showMore').addClass('no-more').find('a').html('No More');
				dish2 = '';
			}
    		dish = '';
			new Masonry( '#menu .row', {
				itemSelector: '.dish-block',
				transitionDuration : 0
			} );
			
			$('#menu .dish-block').each(function(){
				if(!$(this).hasClass('animate')){
					$(this).addClass('animate');
				}
			});
			$('.zoom').magnificPopup({
				type: 'image',
				gallery: {
					enabled: true
				},
				zoom: {
					enabled: true,
					duration: 300
				}
			});
		}, 2500)
		
		return false;
    })

    $('#showMorePosts a').on('click', function(){
    	if(dishPost.length){
    		$('#gridPost').append(dishPost);	
    	}else if(!dishPost.length) {
    		return false
    	}

		setTimeout(function(){
			$('#showMorePosts').addClass('no-more').find('a').html('No More');

    		dishPost = '';
			
			$('#gridPost > div').each(function(){
				if(!$(this).hasClass('animate')){
					$(this).addClass('animate');

					$(this).addClass('fadeInUp animated').css('opacity',1);
					$(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
						$(this).removeClass('fadeInUp animated');
					});		
				}
			});
		}, 1000)
		
		return false;
    })
    /*----------  MOBILE DETECT  ----------*/
    var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};
	/*----------  //MOBILE DETECT  ----------*/
	
	/*----------  PRELOADER  ----------*/
	setTimeout(function(){
		$('#preloader').animate({'opacity' : '0'},300,function(){
			$('#preloader').hide();
			if(0 < $(window).scrollTop()){				
				setTimeout(function(){
					scrolling();
				}, 500)
			}	
		});

		$('.page-wrapper').animate({'opacity' : '1'},500);
	},3000)
	/*----------  //PRELOADER  ----------*/
	
	/*----------  FUNCTION FOR SWITCH THEME COLOR  ----------*/
	if($('.picker-btn').length){
		$('.picker-btn').on('click', function(){
			if(parseInt($('.color-picker').css('right')) == 0){
				$('.color-picker').stop().animate({'right': -160}, 500);
			}else{
				$('.color-picker').stop().animate({'right': 0}, 500);
			}
		});
		$('.color-picker .pwrapper div.color').on('click', function(){
			$('body').removeClass('lightgreen blue green lightred red yellow turquoise pink purple');
			$('body').addClass($(this).attr('data-color'));
		});
	}
	/*----------  //FUNCTION FOR SWITCH THEME COLOR  ----------*/


	$('.images-bg').each(function(){
		$(this).css({
			'background-image': 'url(' +$('>img', this).hide().attr('src') +')'
		});
	});

	setTimeout(function(){
		/*----------  BIG SLIDER  ----------*/

		$('.big-slider .flexslider').flexslider({
			slideshowSpeed: 6000,
			before : function(slider){
				$('.slider-block div', slider).css('opacity', 0);
				var time = -200;
				var time2 = -200;
				$('.flex-active-slide', slider).before().find('.slider-block div').css('opacity', 1).each(function(){		
					var thiz = $(this);						
					time += 200;
					setTimeout(function(){
						thiz.addClass('fadeOutLeft animated');		
						thiz.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
							thiz.removeClass('fadeOutLeft animated').css('opacity', 0);
						});					
					}, time);
				});	
				setTimeout(function(){
					$('.flex-active-slide .slider-block div', slider).each(function(){		
						var thiz = $(this);	
						time2 += 200;
						setTimeout(function(){
							thiz.addClass('fadeInRight animated').css('opacity',1);
							thiz.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
								thiz.removeClass('fadeInRight animated');
							});				
						}, time2);
					});
				}, 500);
			}
		});

		/*----------  //BIG SLIDER  ----------*/

		/*----------  FEEDBACK SLIDER  ----------*/
		$('.feedback .flexslider').flexslider({slideshowSpeed: 6000});
		/*----------  //FEEDBACK SLIDER  ----------*/
	}, 2000);
	
	if($('.zoom').length){
		$('.zoom').magnificPopup({
			type: 'image',
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				duration: 300
			}
		});
	}	

	$('.navbar-link-bg').each(function(){
		var width = $(this).width();
		$(this).css({
			'border-left': width/2 + 'px solid transparent',
			'border-right': width/2 + 'px solid transparent'
		})
		$('span', this).width(width).css('left', width/2*-1);
	})
	/*----------  VIDEO  ----------*/
	var min_w = 300; // minimum video width allowed
    var vid_w_orig;  // original video dimensions
    var vid_h_orig;

	setTimeout(function(){	        
        vid_w_orig = parseInt($('#video-container video').attr('width'));
        vid_h_orig = parseInt($('#video-container video').attr('height'));
        
        $(window).resize(function () { resizeToCover(); });
        $(window).trigger('resize');
    }, 2000);

    function resizeToCover() {

	    // set the video viewport to the window size
	    $('#video-container').width($(window).width());
	    $('#video-container').height($(window).height());

	    // use largest scale factor of horizontal/vertical
	    var scale_h = $(window).width() / vid_w_orig;
	    var scale_v = $(window).height() / vid_h_orig;
	    var scale = scale_h > scale_v ? scale_h : scale_v;

	    // don't allow scaled width < minimum video width
	    if (scale * vid_w_orig < min_w) {scale = min_w / vid_w_orig;};

	    // now scale the video
	    $('#video-container video').width(scale * vid_w_orig);
	    $('#video-container video').height(scale * vid_h_orig);
	    // and center it by scrolling the video viewport
	    $('#video-container').scrollLeft(($('#video-container video').width() - $(window).width()) / 2);
	    $('#video-container').scrollTop(($('#video-container video').height() - $(window).height()) / 2);

    }

    /*----------  //VIDEO  ----------*/
	function scrolling(){
		var scroll = $('.page-wrapper').scrollTop();

		/*----------  HEADER  ----------*/
		if($('.page-wrapper').scrollTop() > 160){
			$('.menu-button').addClass('fixed');
		}else{
			$('.menu-button').removeClass('fixed');
		}
		/*----------  //HEADER  ----------*/

		

		/*----------  ANIMATION FOR ABOUT  ----------*/
		if($('#about').length && parseInt($('#about .row:eq(1)').offset().top)-shift+scroll < scroll){			
			if($('#about .row:eq(1)').hasClass('animate')){
				$('#about .row:eq(1)').removeClass('animate');

				$('#about .row:eq(1) .about-foto').addClass('fadeInRight animated').css('opacity',1);
				$('#about .row:eq(1) .about-foto').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$('#about .row:eq(1) .about-foto').removeClass('fadeInRight animated');
				});	

				$('#about .row:eq(1) .about-text').addClass('fadeInLeft animated').css('opacity',1);
				$('#about .row:eq(1) .about-text').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$('#about .row:eq(1) .about-text').removeClass('fadeInLeft animated');
				});	
			}
		}
		if($('#about').length && parseInt($('#about .row:eq(2)').offset().top)-shift+scroll < scroll){			
			if($('#about .row:eq(2)').hasClass('animate')){
				$('#about .row:eq(2)').removeClass('animate');

				$('#about .row:eq(2) .about-foto').addClass('fadeInLeft animated').css('opacity',1);
				$('#about .row:eq(2) .about-foto').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$('#about .row:eq(2) .about-foto').removeClass('fadeInLeft animated');
				});	

				$('#about .row:eq(2) .about-text').addClass('fadeInRight animated').css('opacity',1);
				$('#about .row:eq(2) .about-text').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$('#about .row:eq(2) .about-text').removeClass('fadeInRight animated');
				});	
			}
		}
		if($('#about').length && parseInt($('#about .row:eq(3)').offset().top)-shift+scroll < scroll + 250){			
			if($('#about .row:eq(3)').hasClass('animate')){
				$('#about .row:eq(3)').removeClass('animate');

				var time = -200;
				$('#about .row:eq(3) .gallery .image-wrapper').each(function(){
					time += 200;
					var thiz = this;
					setTimeout(function(){
						$(thiz).addClass('fadeInUp animated').css('opacity',1);
						$(thiz).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
							$(thiz).removeClass('fadeInUp animated');
						});						
					}, time)
				})

				
			}
		}
		/*----------  //ANIMATION FOR ABOUT  ----------*/

		if($('#menu').length && parseInt($('#menu').offset().top)-shift+scroll < scroll){			
			if($('#menu').hasClass('animate')){
				$('#menu').removeClass('animate');

				var time = -200;
				$('#menu .dish-block').each(function(){
					time += 200;
					var thiz = this;
					setTimeout(function(){
						$(thiz).addClass('animate');						
					}, time)
				})

				
			}
		}

		if($('#facts').length && parseInt($('#facts').offset().top)-shift+scroll < scroll){
			if($('#facts').hasClass('animate')){
				$('#facts').removeClass('animate');

				$('#facts .amount').each(function(){
					$(this).countTo({
	        	        from: 0,
	        	        to: $(this).attr('data-amount'),
	        	        speed: 3000
	                });
				})
				
			}
		}

		if($('#events').length && parseInt($('#events').offset().top)-shift+scroll < scroll){	
			
			if($('#events').hasClass('animate')){
				$('#events').removeClass('animate');

				var time = -200;
				$('#events .row > div').each(function(){
					time += 200;
					var thiz = this;
					setTimeout(function(){
						$(thiz).addClass('fadeInUp animated').css('opacity',1);
						$(thiz).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
							$(thiz).removeClass('fadeInUp animated');
						});						
					}, time)
				})

				
			}
		}
		
	}


	$('.menu a').on('click', function(){
		$('.page-wrapper')
			.stop()
			.scrollTo($($(this).attr('data-scroll')).offset().top+$('.page-wrapper').scrollTop()-110, 300);
	});


	/*----------  FUNCTION FOR WINDOW SCROLL  ----------*/
	$('.page-wrapper').on('scroll', function(){
		scrolling();
	});
	/*----------  //FUNCTION FOR WINDOW SCROLL  ----------*/

    /*----------  WORKS SLIDER  ----------*/
	setTimeout(function(){

		if($('.dish-block').length){
			var $container = $('#menu .row');
			
			$container.imagesLoaded( function() {
		  		$container.masonry({
					itemSelector: '.dish-block',
					transitionDuration : 0
				} );
			});
		}
		
	}, 2000);
	/*----------  //WORKS SLIDER  ----------*/

	/*----------  Date Picker  ----------*/
	$('.form-datetime').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian:0,
		pickerPosition: "bottom-left"
    });
	$('.form-datetime-wrapper .fa-times').on('click', function(){
		$('.form-datetime').val('');
	})	
	/*----------  //Date Picker  ----------*/ 

	/*----------  SUBMIT FUNCTION  ----------*/
    $('.contacts form .button').on('click', function(){
		var thiz = this;
		var flag = true;

		
        if(/\D/.test($('.contacts input[name="phone"]').val()) || !$('.contacts input[name="phone"]').val().length){
            $('.contacts input[name="phone"]').val('').attr('placeholder','please enter phone number').addClass('error');
            flag = false;
        }
        if(/\D/.test($('.contacts input[name="amount"]').val()) || !$('.contacts input[name="amount"]').val().length){
            $('.contacts input[name="amount"]').val('').attr('placeholder','please enter people amount').addClass('error');
            flag = false;
        }
        if(!/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/.test($('.contacts input[name="email"]').val())){
            $('.contacts input[name="email"]').val('').attr('placeholder','please enter correct e-mail').addClass('error');;
            flag = false;
        }

        if(flag){
            $(thiz).parents('form').submit(); 
            $(thiz).addClass('success').html('success'); 
            $('.contacts input').removeClass('error');      	
        }else{
        	$(thiz).addClass('error').html('error'); 
        }

        setTimeout(function(){
        	$(thiz).removeClass('error success').html('send message'); 
        }, 3000)
        
        return false;
    });
	$("#ajax-contact-form").submit(function() {
		var str = $(this).serialize();		
		var href = location.href.replace(/index\.html/g,'');

		$.ajax({
			type: "POST",
			url: href + "contact_form/contact_process.php",
			data: str,
			success: function(msg) {
				// Message Sent - Show the 'Thank You' message and hide the form
				if(msg == 'OK') {
					$(this).addClass('success').find('span:eq(1)').html('success'); 
				} else {
					$(this).addClass('error').find('span:eq(1)').html('error'); 
				}
			}
		});
		return false;
	});
	/*----------  //SUBMIT FUNCTION  ----------*/
	
})(jQuery); 

